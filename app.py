import os
from flask import Flask, render_template, request, redirect, url_for, session, flash
import database as db


app = Flask(__name__, static_folder='static')
app.secret_key = '42211eb178b9818b1aa37598fb79ae3547b5b8c89a30ec2b3a5fdcaa2f6595de'


database = db.Database('data/data.db')
database.setup()


ADMIN_LOGIN_FILE = 'data/admin_login'


def ensure_admin_login() -> None:
    if os.path.exists(ADMIN_LOGIN_FILE):
        return
    with open(ADMIN_LOGIN_FILE, 'w', encoding='utf-8') as file:
        file.write('admin\nadmin\n')


ensure_admin_login()


@app.route('/')
def index():
    return redirect(url_for('login'))


def is_admin(email: str, password: str) -> bool:
    with open(ADMIN_LOGIN_FILE, 'r', encoding='utf-8') as file:
        lines = file.read().splitlines()
        file.close()
        return email == lines[0] and password == lines[1]


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email, password = request.form['email'], request.form['password']
        if is_admin(email, password):
            return redirect(url_for('admin'))

        try:
            customer_id = db.login(database, email, password)
            flash('Login successful!')
            session['customer_id'] = customer_id
            return redirect(url_for('place_order'))
        except ValueError as err:
            error = str(err)
            flash(error, category='error')

    return render_template('login.html')


@app.route('/place-order', methods=['GET', 'POST'])
def place_order():
    if request.method == 'POST':
        try:
            order_id = db.place_order(database, session['customer_id'], request.form)
            return redirect(url_for('track_order', order_id=order_id))
        except ValueError as err:
            flash(str(err), category='error')

    return render_template('place_order.html')


@app.route('/track-order/<int:order_id>', methods=['GET'])
def track_order(order_id: int):
    context = db.Order.get_info(database, order_id)
    return render_template('track_order.html', **context)


@app.route('/admin', methods=['GET', 'POST'])
def admin():
    order = db.Order.get_active(database)[-1]
    if request.method == 'POST':
        status = request.form['status']
        order.update_status(database, status)
        if status == 'completed':
            return render_template('empty_admin.html')

    info = order.get_info(database, order.row_id)
    return render_template('admin.html', **info)


if __name__ == '__main__':
    app.run()
