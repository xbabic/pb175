#!/bin/bash

python3 -m pip install venv
python3 -m venv env
. env/bin/activate
python3 -m pip install -r requirements.txt
