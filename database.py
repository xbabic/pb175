import sqlite3
import re
import hashlib
from typing import Tuple, Optional, Dict, List, Any
import time
import csv
from werkzeug.datastructures import ImmutableMultiDict


MENU_FILE = 'data/menu.csv'
STATUS_CREATED = "created"
STATUS_CONFIRMED = "confirmed"
STATUS_COMPLETED = "completed"


class Database:
    __slots__ = ["path"]

    def __init__(self, path: str):
        self.path = path

    def connect(self) -> Tuple[sqlite3.Connection, sqlite3.Cursor]:
        con = sqlite3.connect(self.path)
        return con, con.cursor()

    def clear(self) -> None:
        con, cur = self.connect()

        cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
        table_names = [row[0] for row in cur.fetchall()]

        for table_name in table_names:
            cur.execute(f"DELETE FROM {table_name};")
        con.commit()

    def setup(self) -> None:
        con, cur = self.connect()

        res = cur.execute('SELECT name FROM sqlite_master')
        tables = [t[0] for t in res.fetchall()]

        if 'customer' not in tables:
            cur.execute('CREATE TABLE customer(email TEXT, password_hash TEXT)')
        if 'order_' not in tables:
            cur.execute(
                'CREATE TABLE order_(status TEXT, time_created INTEGER, customer_id INTEGER)'
            )
        if 'menu_item' not in tables:
            cur.execute('CREATE TABLE menu_item(name TEXT, description TEXT, price INTEGER)')
        if 'order_item' not in tables:
            cur.execute(
                'CREATE TABLE order_item(order_id INTEGER, item_id INTEGER, count INTEGER)'
            )

        menu = cur.execute('SELECT * FROM menu_item').fetchall()
        if not menu:
            MenuItem.setup_menu(self)
        con.commit()


class Customer:
    """ ( email, password hash ) """
    __slots__ = ["row_id", "email", "pass_hash"]

    def __init__(self, email: str, pass_hash: str, row_id: int = -1) -> None:
        self.row_id, self.email, self.pass_hash = row_id, email, pass_hash

    @classmethod
    def get(cls, data: Database, email: str) -> Optional['Customer']:
        _, cur = data.connect()
        res = cur.execute(f"SELECT *, rowid FROM customer WHERE email = '{email}'").fetchall()
        return cls(*res[0]) if res else None

    def write(self, data: Database) -> int:
        con, cur = data.connect()
        cur.execute(f'INSERT INTO customer VALUES(\'{self.email}\', \'{self.pass_hash}\')')
        con.commit()
        self.row_id = cur.lastrowid
        return self.row_id


class Order:
    """ ( status, time created, customer_id ) """
    __slots__ = ['status', 'time_created', 'customer_id', 'row_id']

    def __init__(self, status: str, time_created: int,
                 customer_id: int, row_id: int = -1) -> None:
        self.status = status
        self.time_created = time_created
        self.customer_id = customer_id
        self.row_id = row_id

    @classmethod
    def get(cls, data: Database, customer_id: int, time_created: int) -> Optional['Order']:
        _, cur = data.connect()
        res = cur.execute(
            f"SELECT *, rowid FROM order_\n"
            f"WHERE customer_id = {customer_id} AND time_created = {time_created}"
        ).fetchall()
        return cls(*res[0]) if res else None

    @classmethod
    def get_active(cls, data: Database) -> List['Order']:
        _, cur = data.connect()
        res = cur.execute("select *, rowid from order_ where status != 'completed'").fetchall()
        return [cls(*row) for row in res]

    def write(self, data: Database) -> int:
        con, cur = data.connect()
        cur.execute(f'INSERT INTO order_ VALUES(\'{self.status}\','
                    f'{self.time_created}, {self.customer_id})')
        con.commit()
        self.row_id = cur.lastrowid
        return self.row_id

    def update_status(self, data: Database, status: str) -> None:
        assert self.row_id != -1
        con, cur = data.connect()
        cur.execute(f"update order_ set status = '{status}' where rowid = {self.row_id}")
        con.commit()

    def calculate_price(self, data: Database) -> int:
        _, cur = data.connect()
        res = cur.execute(f"select * from order_item where order_id = {self.row_id}").fetchall()
        price = 0
        for _, menu_item_id, count in res:
            menu_item = MenuItem.from_id(data, menu_item_id)
            price += menu_item.price * count
        return price

    def get_counts(self, data: Database) -> List[int]:
        _, cur = data.connect()
        res = cur.execute(f"select * from order_item\n"
                          f"where order_id = {self.row_id}\n"
                          f"order by item_id").fetchall()
        counts = [0] * 4
        for _, item_id, count in res:
            counts[item_id - 1] = count
        return counts

    @classmethod
    def get_info(cls, data: Database, row_id: int) -> Dict[str, Any]:
        _, cur = data.connect()
        res = cur.execute(f"select *, rowid from order_ where rowid = {row_id}").fetchall()
        order = cls(*res[0])
        return {
            "counts": order.get_counts(data),
            "total_price": order.calculate_price(data),
            "time_created": time.strftime("%d.%m.%Y %H:%M", time.localtime(order.time_created)),
            "status": order.status
        }


class MenuItem:
    """ ( name, description, price ) """
    __slots__ = ['name', 'description', 'price', 'row_id']

    def __init__(self, name: str, description: str, price: int, row_id: int = -1):
        self.name, self.description, self.price, self.row_id = name, description, price, row_id

    def write(self, data: Database) -> int:
        con, cur = data.connect()
        cur.execute(
            f"INSERT INTO menu_item VALUES('{self.name}', '{self.description}', '{self.price}')")
        con.commit()
        self.row_id = cur.lastrowid
        return self.row_id

    @classmethod
    def from_id(cls, data: Database, row_id: int) -> 'MenuItem':
        _, cur = data.connect()
        res = cur.execute(f"select * from menu_item where rowid = {row_id}").fetchall()
        return cls(*res[0])

    @classmethod
    def setup_menu(cls, data: Database) -> None:
        with open(MENU_FILE, 'r', encoding='utf-8') as file:
            reader = csv.reader(file, delimiter=';')
            for name, desc, price in reader:
                cls(name, desc, int(price)).write(data)


class OrderItem:
    """ ( order_id, item_id, count ) """
    __slots__ = ['order_id', 'menu_item_id', 'count', 'row_id']

    def __init__(self, order_id: int, menu_item_id: int, count: int, row_id: int = -1):
        self.order_id, self.menu_item_id, self.count, self.row_id = \
            order_id, menu_item_id, count, row_id

    def write(self, data: Database) -> int:
        con, cur = data.connect()
        cur.execute(
            f"INSERT INTO order_item VALUES({self.order_id}, {self.menu_item_id}, {self.count})")
        con.commit()
        self.row_id = cur.lastrowid
        return self.row_id


def login(data: Database, email: str, password: str) -> int:
    if not re.match(r'^[\w.-]+@([\w-]+\.)+[\w-]{2,4}$', email):
        raise ValueError('Invalid email!')
    if not password:
        raise ValueError('Empty password!')

    pass_hash = hashlib.sha256(bytes(password, 'utf-8'),
                               usedforsecurity=True).hexdigest()
    cust = Customer.get(data, email)
    if cust:
        if cust.pass_hash != pass_hash:
            raise ValueError('Wrong password!')
        return cust.row_id

    return Customer(email, pass_hash).write(data)


def place_order(data: Database, customer_id: int, form: ImmutableMultiDict) -> int:
    if not any(form.values()):
        raise ValueError("No items in order!")

    order = Order(STATUS_CREATED, int(time.time()), customer_id)
    order.write(data)

    for key, val in form.items():
        if not val:
            continue
        item_id = int(key[1:])
        count = int(val)
        OrderItem(order.row_id, item_id, count).write(data)

    return order.row_id
