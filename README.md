## Requirements
* Software:
  * UNIX-like OS
  * `bash`
  * `python3` version 3.9 and above
  * `pip`
  * `sqlite3`
  * `git`
* Hardware:
  * 1GB RAM
  * 1GB disk space

## Installation
Clone this repository.
```shell
# with HTTPS,
git clone https://gitlab.fi.muni.cz/xbabic/pb175.git
```
```shell
# or with SSH
git clone git@gitlab.fi.muni.cz:xbabic/pb175.git
```

Go inside it and run the installation script.
```shell
cd pb175
bash install.sh
```

## Running the App
Make sure you're inside the project root directory and execute the run script.
```shell
cd pb175
bash run.sh
```
